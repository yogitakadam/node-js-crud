module.exports=(sequelize, Sequelize)=>
{
    const Customer=sequelize.define("customer",{
      fullname:{
        type: Sequelize.STRING
      },
      address:{
        type: Sequelize.STRING
      }
    });
    return Customer;
};