const express = require("express");
const cors = require("cors");


const app = express();


var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

const db = require("./models");
const { customers } = require("./models");
db.sequelize.sync();


// simple route
app.get("/", (req, res) => {
  //console.log(req);
  res.json({ message: "Welcome to MSSQL-Node application." });
  //console.log(res);
});

require("./routes/routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
// const Customers=db.customers;

// customers.sync.then(()=>{

//   customers.bulkCreate([
//     {
//       id:1,
//       fullname:"test",
//       address:"XYZ"
//     },
//     {
//       id:2,
//       fullname:"test",
//       address:"XYZ"
//     }]
//   )
// });